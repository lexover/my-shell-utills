#!/bin/bash
# 03.12.2012 Striganov Sergey 
# www.striganov.com
# Cкрипт чтения новостей яндекс новости через RHVoice

# Склоняем в соответствующем падеже слова "час" и "минута"
check_date()
{
CURR_HOUR=`date +%H`
CURR_MIN=`date +%M`
TEMP_HOUR="`echo $CURR_HOUR | colrm 1 1`"
TEMP_MIN="`echo $CURR_MIN | colrm 1 1`"

if [ "$CURR_HOUR" = "0" ] || [ "$CURR_HOUR" = "5" ] || [ "$CURR_HOUR" = "6" ] || [ "$CURR_HOUR" = "7" ] || [ "$CURR_HOUR" = "8" ] || [ "$CURR_HOUR" = "9" ] || [ "$CURR_HOUR" = "11" ] || [ "$CURR_HOUR" = "12" ] || [ "$CURR_HOUR" = "13" ] || [ "$CURR_HOUR" = "14" ] || [ "X$TEMP_HOUR" = "X0" ] || [ "X$TEMP_HOUR" = "X5" ] || [ "X$TEMP_HOUR" = "X6" ] || [ "X$TEMP_HOUR" = "X7" ] || [ "X$TEMP_HOUR" = "X8" ] || [ "X$TEMP_HOUR" = "X9" ]; then LC_HOUR="часов" # "X" добавлен на тот случай, если переменная TEMP_HOUR - пустая
elif [ "$CURR_HOUR" = "2" ] || [ "$CURR_HOUR" = "3" ] || [ "$CURR_HOUR" = "4" ] || [ "X$TEMP_HOUR" = "X2" ] || [ "X$TEMP_HOUR" = "X3" ] || [ "X$TEMP_HOUR" = "X4" ]; then LC_HOUR="час+а"
elif [ "$CURR_HOUR" = "1" ] || [ "X$TEMP_HOUR" = "X1" ]; then LC_HOUR="час"
fi

if [ "$CURR_MIN" = "5" ] || [ "$CURR_MIN" = "6" ] || [ "$CURR_MIN" = "7" ] || [ "$CURR_MIN" = "8" ] || [ "$CURR_MIN" = "9" ] || [ "$CURR_MIN" = "11" ] || [ "$CURR_MIN" = "12" ] || [ "$CURR_MIN" = "13" ] || [ "$CURR_MIN" = "14" ] || [ "X$TEMP_MIN" = "X0" ] || [ "X$TEMP_MIN" = "X5" ] || [ "X$TEMP_MIN" = "X6" ] || [ "X$TEMP_MIN" = "X7" ] || [ "X$TEMP_MIN" = "X8" ] || [ "X$TEMP_MIN" = "X9" ]; then LC_MINUTE="минут"
elif [ "$CURR_MIN" = "3" ] || [ "$CURR_MIN" = "4" ] || [ "X$TEMP_MIN" = "X3" ] || [ "X$TEMP_MIN" = "X4" ]; then LC_MINUTE="минуты"
fi

if [ "$CURR_MIN" = "1" ]; then DATE_TIME="$CURR_HOUR $LC_HOUR однa минута"
elif [ "$CURR_MIN" = "11" ]; then DATE_TIME="$CURR_HOUR $LC_HOUR $CURR_MIN $LC_MINUTE"
elif [ "X$TEMP_MIN" = "X1" ]; then DATE_TIME="$CURR_HOUR $LC_HOUR $(echo $CURR_MIN-1|bc) одна минута"
elif [ "$CURR_MIN" = "2" ]; then DATE_TIME="$CURR_HOUR $LC_HOUR две минуты"
elif [ "$CURR_MIN" = "12" ]; then DATE_TIME="$CURR_HOUR $LC_HOUR $CURR_MIN $LC_MINUTE"
elif [ "X$TEMP_MIN" = "X2" ]; then DATE_TIME="$CURR_HOUR $LC_HOUR $(echo $CURR_MIN-2|bc) две минуты"
elif [ "$CURR_MIN" = "00" ]; then DATE_TIME="$CURR_HOUR $LC_HOUR ровно"
else DATE_TIME="$CURR_HOUR $LC_HOUR $CURR_MIN $LC_MINUTE"
fi
}

# Получаем Яндекс Новости (Главные)
TMP_FILE="/tmp/yandex-general.rss"
#URL="http://news.yandex.ru/index.rss"
URL="http://sharij.net/feed"
wget -q -O - $URL > $TMP_FILE

#Распарсим и подготовим к чтению Яндекс Новости
NEWS_YA_GENERAL="`cat $TMP_FILE | tr -d '\n'| sed 's/:/, /g' | sed 's/«/\"/g' | sed 's/»/\"/g' | sed 's/\./ /g'  | sed 's/<title>/\n<title>/g' | sed 's/<\/title>/. \n<\/title>/g' | grep -e '<title>' |  sed -e 's/<title>//' | sed '1,1d'`"

check_date 

# а теперь говорим - смотря какая система у Вас установлена: либо RHVoice либо festival
#echo  "Время  $DATE_TIME. Прослушайте свежие $NEWS_YA_GENERAL.  Вот и все новости, спасибо за внимание." | festival --tts --language russian
#echo  "Время  $DATE_TIME. Прослушайте свежие новости. $NEWS_YA_GENERAL.  Вот и все новости, спасибо за внимание." | RHVoice |  aplay -q
spd-say -l ru  "Время  $DATE_TIME. Прослушайте свежие новости. $NEWS_YA_GENERAL.  Вот и все новости, спасибо за внимание."
