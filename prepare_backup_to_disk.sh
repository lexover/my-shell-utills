#!/bin/bash
###########################################################################################
# Этот скрипт выполняет копирование файлов (с сохранением структуры дерева директорий) из
# директории DIR в директорию BACKUP_DIR с проверкой на наличие/отстутствие файла в списке
# file_list.csv который содержит список файлов ранее записанных на диск, те файлы которые
# есть в списке скопированы не будут. 
# Для корректной работы скрипта необходимо наличие файла file_list.csv в директории с 
# текущим файлом. Данный файл формируется экспортом в csv файл имен файлов из программ
# каталогизаторов дисков CDCat или Tellico
###########################################################################################
index=0
count=0
# Здесь нужно установить папку в соответствии с которой
DIR='/media/lexover/Media/Личное/Видео/Черновик/'
BACKUP_DIR='/media/lexover/Media/Видео_Черновики_Backup/'
FILE_LIST='file_list.csv'
IFS=$'\n'

DATA=`cat $FILE_LIST`

function isExist()
{
  result=`echo "$DATA" | grep "$1"`
  if [[ -n $result ]];
  then
    return 1
  fi
  return 0
}

function createPath()
{
   if ! [ -d  "$1" ]; then
     mkdir -p "$1"
     echo "mkdir $1"
   fi
}

find "$DIR" -type f | while read FULL_FILENAME
do
  DI="${FULL_FILENAME/$DIR/''}"
  FILE_NAME=`echo "$DI" | sed "s/.*\///"`
  FILE_PATH="$BACKUP_DIR${DI/$FILE_NAME/''}"

  isExist $FILE_NAME
  return_val=$?
  if [ "$return_val" -eq "1" ]; then
    count=$(($count+1))
    echo "Exist $count"
  else
    createPath $FILE_PATH
    if ! [ -f "$FILE_PATH$FILE_NAME" ]
    then 
      cp "$FULL_FILENAME" "$FILE_PATH$FILE_NAME"
      echo "cp $FILE_NAME"
    else
      echo "$FILE_NAME already exists in backup dir"
    fi
  fi
done
echo "Finished copy elements"
