#########################################################################
# Считывает из текстового файла список контактов разделенных запятой    # 
# в формате "имя контакта <e-mail@mail.com>, имя....."                  #
# создает рядом с указанным файлом файл с тем же именем но расшир .vcf  #
# который оформлен в формате vCard 3.0 и может быть импортирован        #
# для дальнейшего использования.                                        #
#########################################################################

#!/bin/bash

echo -n "Endter path to file with mails > "
read MAILS_PATH
echo "File with mails path: $MAILS_PATH"

MAILS_LIST=`cat $MAILS_PATH`
VCF_PATH="$MAILS_PATH.vcf"

IFS=,
ary=($MAILS_LIST)

`touch "$VCF_PATH"`

for key in "${!ary[@]}"
do
  echo "BEGIN:VCARD" >> "$VCF_PATH"
  echo "VERSION:3.0" >> "$VCF_PATH"
  N1=`echo "${ary[$key]}" | awk -F "<|>" '{print $1}'`
  N2=${N1#" "}
  echo "FN:$N2" >> "$VCF_PATH"
  NAME=${N2//' '/';'}
  echo "N:$NAME" >> "$VCF_PATH"
  MAIL=`echo "${ary[$key]}" | awk -F "<|>" '{print $2}'`
  echo "EMAIL;TYPE=INTERNET:$MAIL" >> "$VCF_PATH"
  echo "END:VCARD" >> "$VCF_PATH"
  echo "" >> "$VCF_PATH"
  echo "$key : $N2 - $MAIL"
done
